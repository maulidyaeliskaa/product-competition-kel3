import axios from 'axios';
import Cookies from 'js-cookie';

const DEVELOPMENT = process.env.REACT_APP_BE_URL;

const api = axios.create({
  baseURL: DEVELOPMENT,
});

api.interceptors.request.use(
  (config) => {
    const token = Cookies.get('token');

    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

api.interceptors.response.use(
  (config) => {
    return config;
  },
  (error) => {
    if (error.response) {
      const { status } = error.response;
      if (status === 401) {
        window.location.replace('/logout');
      }
    }

    return Promise.reject(error);
  }
);

export default api;
