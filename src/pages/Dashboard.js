import Cookies from "js-cookie";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import api from "../api/api";
import BarChart from "../component/BarChart";
import Chart from "../component/Chart";
import CompletedResearch from "../component/CompletedResearch";
import InputResearch from "../component/InputResearch";
import LineDiagram from "../component/LineDiagram";
import Manager from "../component/Manager";
import Navbar from "../component/Navbar";
import Progress from "../component/Progress";
import ResearchPhaseDetailMain from "../component/ResearchPhaseDetailMain";
import ResearchPhaseMain from "../component/ResearchPhaseMain";
import ResearchPhaseTop from "../component/ResearchPhaseTop";
import Sidebar from "../component/Sidebar";
import Slider from "../component/Slider";
import JudulResearch from "../component/judulResearch";

const Dashboard = () => {
  const [currentPage, setCurrentPage] = useState("Dashboard");
  const [profileDetail, setProfileDetail] = useState(null);
  const [isLoading, setIsLoading] = useState(false);

  const navigate = useNavigate();

  const handleDashboard = () => {
    setCurrentPage("Dashboard");
  };

  const handleResearchDetail = () => {
    setCurrentPage("ResearchDetail");
  };

  const handleResearchDetails = () => {
    setCurrentPage("ResearchDetails");
  };

  const handleResearchPhase = () => {
    setCurrentPage("ResearchPhase");
  };

  const handleResearchPhaseDetail = () => {
    setCurrentPage("ResearchPhaseDetail");
  };

  const handleBudgeting = () => {
    setCurrentPage("Budgeting");
  };

  const handleInitiateResearchDetail = () => {
    setCurrentPage("InitiateResearchDetail");
  };

  const handleAsignManager = () => {
    setCurrentPage("AsignManager");
  };

  useEffect(() => {
    const token = Cookies.get("token");
    if (!token) {
      navigate("/");
      return;
    }

    getDetailUser();
  }, [navigate]);

  const renderCurrentPage = () => {
    switch (currentPage) {
      case "Dashboard":
        return <DashboardDetail />;
      case "ResearchDetail":
        return (
          <ResearchDetail
            handleInitiateResearchDetail={handleInitiateResearchDetail}
            profileDetail={profileDetail}
            handleResearchDetails={handleResearchDetails}
          />
        );
      case "ResearchDetails":
        return <ResearchDetails />;
      case "InitiateResearchDetail":
        return (
          <InitiateResearchDetail
            handleInitiateResearchDetail={handleInitiateResearchDetail}
          />
        );
      case "AsignManager":
        return <AsignManager handleAsignManager={handleAsignManager} />;
      case "ResearchPhase":
        return (
          <ResearchPhase
            handleResearchPhaseDetail={handleResearchPhaseDetail}
          />
        );
      case "ResearchPhaseDetail":
        return <ResearchPhaseDetail />;
      default:
        // Handle default case or return null if no match
        return null;
    }
  };

  async function getDetailUser() {
    setIsLoading(true);
    try {
      const response = await api.get("/user-detail");
      const result = response.data.data;
      setProfileDetail(result);

      localStorage.setItem("profile", JSON.stringify(result));
    } catch (error) {
      console.error(error);
    } finally {
      setIsLoading(false);
    }
  }

  if (isLoading) return <p>Loading ....</p>;

  return (
    <div id="WN9DGJJ1Si">
      <Sidebar
        onDashboardClick={handleDashboard}
        onResearchDetailClick={handleResearchDetail}
        onResearchPhaseClick={handleResearchPhase}
      />
      <div>
        <Navbar profileDetail={profileDetail} />
        {renderCurrentPage()}
      </div>
    </div>
  );
};

function ResearchDetail({
  handleInitiateResearchDetail,
  profileDetail,
  handleResearchDetails,
}) {
  return (
    <>
      <div id="GotANewIdea">
        <svg
          width="66"
          height="66"
          viewBox="0 0 66 66"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          {/* ... SVG path */}
        </svg>
        <p>Got a new Idea?</p>
        {profileDetail.roleId === 3 && (
          <button id="InitiateResearch" onClick={handleInitiateResearchDetail}>
            Initiate a New Research
          </button>
        )}
      </div>
      <Slider handleResearchDetails={handleResearchDetails} />
      <CompletedResearch />
    </>
  );
}

function ResearchDetails() {
  return (
    <>
      <JudulResearch />
      <div id="ResearchDetails">
        <div id="ResearchDetailsLeft">
          <p>Latar Belakang Research</p>
          <p>
            Sosial media telah menjadi salah satu sumber utama informasi di
            masyarakat. Diperlukan penelitian untuk mengetahui hubungan media
            sosial dengan pengaruh politik.
          </p>
          <p>Kategori Research</p>
          <p>Sosial</p>
          <p>GoalsResearch</p>
          <p>Mengetahui pengaruh media sosial terhadap opini politik publik</p>
        </div>
        <Progress />
      </div>
      <div id="ResearchDetailsButton">
        <button>Cancel</button>
        <button>Save</button>
        <button>Download Report</button>
      </div>
    </>
  );
}

function AsignManager({ handleInitiateResearchDetail }) {
  return (
    <>
      <JudulResearch />
      <Manager onInitiateResearchDetailClick={handleInitiateResearchDetail} />
    </>
  );
}

function InitiateResearchDetail({ handleAsignManager }) {
  return <InputResearch onAsignManagerClick={handleAsignManager} />;
}

function DashboardDetail() {
  return (
    <>
      <Slider />
      <div id="ChartDashboard">
        <LineDiagram />
        <Chart />
        <BarChart />
        <Progress />
      </div>
    </>
  );
}

//Research Pharse
function ResearchPhase({ handleResearchPhaseDetail }) {
  return (
    <>
      <ResearchPhaseTop />
      <ResearchPhaseMain
        onResearchPhaseDetailClick={handleResearchPhaseDetail}
      />
    </>
  );
}

function ResearchPhaseDetail() {
  return (
    <>
      <ResearchPhaseDetailMain />
    </>
  );
}

export default Dashboard;
