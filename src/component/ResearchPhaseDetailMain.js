import "bootstrap/dist/css/bootstrap.min.css";
import React, { useState } from "react";

export default function ResearchPhaseDetailMain() {
  const [currentPage, setCurrentPage] = useState("ResearchPhaseDetailMain");

  const renderCurrentPage = () => {
    switch (currentPage) {
      case "ResearchPhaseDetailMain":
        return (
          <>
            <div id="ResearchPhaseDetailMain">
              <p>Judul Research</p>
              <div id="ResearchPhaseDetailMainComponent">
                <div id="ResearchPhaseDetailMainComponentLeft">
                  <div id="MembersComponent">
                    <p>Members</p>
                    <div id="Members">
                      <img
                        src="/assets/images/Avatar.png"
                        alt=""
                        id="AvatarImage"
                      />
                      <img
                        src="/assets/images/Avatar1.png"
                        alt=""
                        id="AvatarImage"
                      />
                      <img
                        src="/assets/images/Avatar2.png"
                        alt=""
                        id="AvatarImage"
                      />
                      <img
                        src="/assets/images/Avatar3.png"
                        alt=""
                        id="AvatarImage"
                      />
                      <p>+5</p>
                    </div>
                  </div>
                  <div id="DurationComponent">
                    <p>Duration</p>
                    <p>11/09/2023 to 25/09/2023</p>
                  </div>
                </div>
                <button>Download Define Phase Report</button>
              </div>
            </div>
            <div id="ResearchPhaseDetailSection">
              <div id="ResearchPhaseDetailActivities">
                <p>Activities</p>
                <div id="ProgressBarActivities">
                  <p>100%</p>
                  <div id="MainProgressBarActivities">
                    <div
                      id="ChildProgressBarActivities"
                      style={{ width: "60%" }}
                    ></div>
                  </div>
                </div>
                <div id="ListSection">
                  <div id="CheckList">
                    <input type="checkbox" checked />
                    <label>Technology scanning</label>
                  </div>
                  <div id="CheckList">
                    <input type="checkbox" checked />
                    <label>Formulating research question & purpose</label>
                  </div>
                  <div id="CheckList">
                    <input type="checkbox" checked />
                    <label>Literature review</label>
                  </div>
                  <div id="CheckList">
                    <input type="checkbox" checked />
                    <label>Create hypothesis</label>
                  </div>
                  <div id="CheckList">
                    <input type="checkbox" checked />
                    <label>User research</label>
                  </div>
                  <div id="CheckList">
                    <input type="checkbox" checked />
                    <label>Market research</label>
                  </div>
                  <div id="CheckList">
                    <input type="checkbox" checked />
                    <label>Competitor research</label>
                  </div>
                  <div id="CheckList">
                    <input type="checkbox" checked />
                    <label>Internal capability research</label>
                  </div>
                  <div id="CheckList">
                    <input type="checkbox" checked />
                    <label>Business opportunity research</label>
                  </div>
                </div>
              </div>
              <div id="ResearchPhaseDetailKeyResult">
                <p>Key Result</p>
                <div id="ListKeyResult">
                  <p>&#62;1 research question defined</p>
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Aliquam eu vehicula nisi. Nunc et rhoncus enim.
                  </p>
                </div>
                <div id="ListKeyResult">
                  <p>5 papers reviewed</p>
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Aliquam eu vehicula nisi. Nunc et rhoncus enim.
                  </p>
                </div>
                <div id="ListKeyResult">
                  <p>Targeted FU/CFU defined</p>
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Aliquam eu vehicula nisi. Nunc et rhoncus enim.
                  </p>
                </div>
                <div id="ListKeyResult">
                  <p>Project research charter</p>
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Aliquam eu vehicula nisi. Nunc et rhoncus enim.
                  </p>
                </div>
              </div>
              <div id="ResearchPhaseDetailDoc">
                <p>Dokumen Pendukung</p>
                <a href="#">Judul Research - Define Report.docx</a>
                <a href="#">Field Report.pdf</a>
                <a href="#">Evidence.jpg</a>
              </div>
              <div id="ResearchPhaseDetailComment">
                <p>Komentar</p>
                <div>
                  <img src="/assets/images/Photo.png" alt="" />
                  <p>Reno : Mohon review dan approval, thanks</p>
                </div>
                <div>
                  <img src="/assets/images/Photo1.png" alt="" />
                  <p>Hedi : Oke, lanjutkan</p>
                </div>
              </div>
            </div>
          </>
        );
      case "ResearchPhaseDetailDesign":
        return (
          <>
            <div id="ResearchPhaseDetailMainDesign">
              <p>Judul Research</p>
              <div id="ResearchPhaseDetailMainComponentDesign">
                <div id="ResearchPhaseDetailMainComponentLeftDesign">
                  <div id="MembersComponentDesign">
                    <p>Members</p>
                    <div id="MembersDesign">
                      <img
                        src="/assets/images/Avatar.png"
                        alt=""
                        id="AvatarImageDesign"
                      />
                      <img
                        src="/assets/images/Avatar1.png"
                        alt=""
                        id="AvatarImageDesign"
                      />
                      <img
                        src="/assets/images/Avatar2.png"
                        alt=""
                        id="AvatarImageDesign"
                      />
                      <img
                        src="/assets/images/Avatar3.png"
                        alt=""
                        id="AvatarImageDesign"
                      />
                      <p>+5</p>
                    </div>
                  </div>
                  <div id="DurationComponentDesign">
                    <p>Duration</p>
                    <p>11/09/2023 to 25/09/2023</p>
                  </div>
                </div>
                <button>Download Define Phase Report</button>
              </div>
            </div>
            <div id="ResearchPhaseDetailSectionDesign">
              <div id="ResearchPhaseDetailSectionDesignEdit">
                <div id="ResearchPhaseDetailActivitiesDesign">
                  <p>Activities</p>
                  <div id="ProgressBarActivitiesDesign">
                    <p>100%</p>
                    <div id="MainProgressBarActivitiesDesign">
                      <div
                        id="ChildProgressBarActivitiesDesign"
                        style={{ width: "60%" }}
                      ></div>
                    </div>
                  </div>
                  <div id="ListSectionDesign">
                    <div id="CheckListDesign">
                      <input type="checkbox" checked />
                      <label>Technology scanning</label>
                    </div>
                    <div id="CheckListDesign">
                      <input type="checkbox" checked />
                      <label>Formulating research question & purpose</label>
                    </div>
                  </div>
                </div>
                <div id="ResearchPhaseDetailDocDesign">
                  <p>Dokumen Pendukung</p>
                  <a href="#">Judul Research - Define Report.docx</a>
                  <a href="#">Field Report.pdf</a>
                  <a href="#">Evidence.jpg</a>
                </div>
              </div>
              <div id="ResearchPhaseDetailSectionDesignEdit">
                <div
                  id="ResearchPhaseDetailKeyResultDesign"
                  style={{ height: "430px" }}
                >
                  <p>Key Result</p>
                  <div id="ListKeyResultDesign">
                    <p>&#62;1 research question defined</p>
                    <p>
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                      Aliquam eu vehicula nisi. Nunc et rhoncus enim.
                    </p>
                  </div>
                  <div id="ListKeyResultDesign">
                    <p>5 papers reviewed</p>
                    <p>
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                      Aliquam eu vehicula nisi. Nunc et rhoncus enim.
                    </p>
                  </div>
                  <div id="ListKeyResultDesign">
                    <p>Targeted FU/CFU defined</p>
                    <p>
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                      Aliquam eu vehicula nisi. Nunc et rhoncus enim.
                    </p>
                  </div>
                </div>
                <div id="ResearchPhaseDetailCommentDesign">
                  <p>Komentar</p>
                  <div>
                    <img src="/assets/images/Photo.png" alt="" />
                    <p>Reno : Mohon review dan approval, thanks</p>
                  </div>
                  <div>
                    <img src="/assets/images/Photo1.png" alt="" />
                    <p>Hedi : Oke, lanjutkan</p>
                  </div>
                </div>
              </div>
            </div>
          </>
        );
      case "ResearchPhaseDetailDevelop":
        return (
          <>
            <div id="ResearchPhaseDetailMainDesign">
              <p>Judul Research</p>
              <div id="ResearchPhaseDetailMainComponentDesign">
                <div id="ResearchPhaseDetailMainComponentLeftDesign">
                  <div id="MembersComponentDesign">
                    <p>Members</p>
                    <div id="MembersDesign">
                      <img
                        src="/assets/images/Avatar.png"
                        alt=""
                        id="AvatarImageDesign"
                      />
                      <img
                        src="/assets/images/Avatar1.png"
                        alt=""
                        id="AvatarImageDesign"
                      />
                      <img
                        src="/assets/images/Avatar2.png"
                        alt=""
                        id="AvatarImageDesign"
                      />
                      <img
                        src="/assets/images/Avatar3.png"
                        alt=""
                        id="AvatarImageDesign"
                      />
                      <p>+5</p>
                    </div>
                  </div>
                  <div id="DurationComponentDesign">
                    <p>Duration</p>
                    <p>11/09/2023 to 25/09/2023</p>
                  </div>
                </div>
                <button>Download Define Phase Report</button>
              </div>
            </div>
            <div id="ResearchPhaseDetailSectionDesign">
              <div id="ResearchPhaseDetailSectionDesignEdit">
                <div
                  id="ResearchPhaseDetailActivitiesDesign"
                  style={{ height: "322px" }}
                >
                  <p>Activities</p>
                  <div id="ProgressBarActivitiesDesign">
                    <p>100%</p>
                    <div id="MainProgressBarActivitiesDesign">
                      <div
                        id="ChildProgressBarActivitiesDesign"
                        style={{ width: "60%" }}
                      ></div>
                    </div>
                  </div>
                  <div id="ListSectionDesign">
                    <div id="CheckListDesign">
                      <input type="checkbox" checked />
                      <label>Technology scanning</label>
                    </div>
                    <div id="CheckListDesign">
                      <input type="checkbox" checked />
                      <label>Formulating research question & purpose</label>
                    </div>
                    <div id="CheckListDesign">
                      <input type="checkbox" checked />
                      <label>Formulating research question & purpose</label>
                    </div>
                    <div id="CheckListDesign">
                      <input type="checkbox" checked />
                      <label>Formulating research question & purpose</label>
                    </div>
                    <div id="CheckListDesign">
                      <input type="checkbox" checked />
                      <label>Formulating research question & purpose</label>
                    </div>
                  </div>
                </div>
                <div id="ResearchPhaseDetailDocDesign">
                  <p>Dokumen Pendukung</p>
                  <a href="#">Judul Research - Define Report.docx</a>
                  <a href="#">Field Report.pdf</a>
                  <a href="#">Evidence.jpg</a>
                </div>
              </div>
              <div id="ResearchPhaseDetailSectionDesignEdit">
                <div
                  id="ResearchPhaseDetailKeyResultDesign"
                  style={{ height: "305px" }}
                >
                  <p>Key Result</p>
                  <div id="ListKeyResultDesign">
                    <p>&#62;1 research question defined</p>
                    <p>
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                      Aliquam eu vehicula nisi. Nunc et rhoncus enim.
                    </p>
                  </div>
                  <div id="ListKeyResultDesign">
                    <p>5 papers reviewed</p>
                    <p>
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                      Aliquam eu vehicula nisi. Nunc et rhoncus enim.
                    </p>
                  </div>
                </div>
                <div id="ResearchPhaseDetailCommentDesign">
                  <p>Komentar</p>
                  <div>
                    <img src="/assets/images/Photo.png" alt="" />
                    <p>Reno : Mohon review dan approval, thanks</p>
                  </div>
                  <div>
                    <img src="/assets/images/Photo1.png" alt="" />
                    <p>Hedi : Oke, lanjutkan</p>
                  </div>
                </div>
              </div>
            </div>
          </>
        );
      default:
        // Handle default case or return null if no match
        return null;
    }
  };

  return (
    <>
      <div id="ResearchPhaseNavbar">
        <svg
          width="18"
          height="18"
          viewBox="0 0 18 18"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M5.66667 13.1668H12.3333M8.18141 1.30345L2.52949 5.69939C2.15168 5.99324 1.96278 6.14017 1.82669 6.32417C1.70614 6.48716 1.61633 6.67077 1.56169 6.866C1.5 7.08639 1.5 7.3257 1.5 7.80433V13.8334C1.5 14.7669 1.5 15.2336 1.68166 15.5901C1.84144 15.9037 2.09641 16.1587 2.41002 16.3185C2.76654 16.5001 3.23325 16.5001 4.16667 16.5001H13.8333C14.7668 16.5001 15.2335 16.5001 15.59 16.3185C15.9036 16.1587 16.1586 15.9037 16.3183 15.5901C16.5 15.2336 16.5 14.7669 16.5 13.8334V7.80433C16.5 7.3257 16.5 7.08639 16.4383 6.866C16.3837 6.67077 16.2939 6.48716 16.1733 6.32417C16.0372 6.14017 15.8483 5.99324 15.4705 5.69939L9.81859 1.30345C9.52582 1.07574 9.37943 0.961888 9.21779 0.918123C9.07516 0.879506 8.92484 0.879506 8.78221 0.918123C8.62057 0.961888 8.47418 1.07574 8.18141 1.30345Z"
            stroke="#9A9A9C"
            stroke-width="1.66667"
            stroke-linecap="round"
            stroke-linejoin="round"
          />
        </svg>
        <svg
          width="6"
          height="10"
          viewBox="0 0 6 10"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M1 9L5 5L1 1"
            stroke="#C8C8C9"
            stroke-width="1.33333"
            stroke-linecap="round"
            stroke-linejoin="round"
          />
        </svg>
        <p
          onClick={() => {
            setCurrentPage("ResearchPhaseDetailMain");
          }}
        >
          Define
        </p>
        <svg
          width="6"
          height="10"
          viewBox="0 0 6 10"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M1 9L5 5L1 1"
            stroke="#C8C8C9"
            stroke-width="1.33333"
            stroke-linecap="round"
            stroke-linejoin="round"
          />
        </svg>
        <p
          onClick={() => {
            setCurrentPage("ResearchPhaseDetailDesign");
          }}
        >
          Design
        </p>
        <svg
          width="6"
          height="10"
          viewBox="0 0 6 10"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M1 9L5 5L1 1"
            stroke="#C8C8C9"
            stroke-width="1.33333"
            stroke-linecap="round"
            stroke-linejoin="round"
          />
        </svg>
        <p
          onClick={() => {
            setCurrentPage("ResearchPhaseDetailDevelop");
          }}
        >
          Develop
        </p>
      </div>
      {renderCurrentPage()}
    </>
  );
}
