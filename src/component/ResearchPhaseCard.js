export default function ResearchPhaseCard({ onResearchPhaseDetailClick }){
    return(
        <>
            <div id="ResearchPhaseCard" onClick={onResearchPhaseDetailClick}>
                <img src="/assets/images/Image.png" alt="" width={326} height={160}/>
                <div>
                    <p>Judul Research</p>
                    <p>Comets are a big source of meteoroids because of the nature of those long tails. A large amount of dust.</p>
                    <div>
                        <img src="/assets/images/Photo.jpg" alt="" width={40} height={40}/>
                        <img src="/assets/images/Photo2.png" alt="" width={40} height={40}/>
                        <img src="/assets/images/Photo3.png" alt="" width={40} height={40}/>
                    </div>
                </div>
            </div>
        </>
    )
}