export default function CompletedResearch() {
  return (
    <div className="wrapper">
      <div className="button-wrapper">
        <button className="button-more">
          More <img src="/assets/images/right-arrow.png"></img>
        </button>
      </div>
      <h2>Completed Research</h2>

      <ul className="carousel">
        <li className="card">
          <div>
            <img src="/assets/images/Image.png"></img>
            <h3>Judul research</h3>
            <p>Comets are a big source of meteoroids because of the nature of those long tails. A large amount of dust.</p>
            <img className="photo" src="/assets/images/Photo3.png"></img>
            <img className="photo" src="/assets/images/Photo2.png"></img>
          </div>
        </li>
      </ul>
    </div>
  );
}
