export default function ResearchPhaseTop(){
    return(
      <div id='ResearchPhaseTop'>
        <div id='ResearchPhaseTopCard'>
          <p>Define</p>
          <div>
            <p>6</p>
            <p>Research</p>
          </div>
        </div>
        <div id='ResearchPhaseTopCardSecondary'>
          <p>Design</p>
          <div>
            <p>3</p>
            <p>Research</p>
          </div>
        </div>
        <div id='ResearchPhaseTopCardSecondary'>
          <p>Develop</p>
          <div>
            <p>1</p>
            <p>Research</p>
          </div>
        </div>
        <div id='ResearchPhaseTopCardSecondary'>
          <p>Deploy</p>
          <div>
            <p>0</p>
            <p>Research</p>
          </div>
        </div>
        <div id='ResearchPhaseTopCardSecondary'>
          <p>Delivery</p>
          <div>
            <p>0</p>
            <p>Research</p>
          </div>
        </div>
      </div>
    )
}