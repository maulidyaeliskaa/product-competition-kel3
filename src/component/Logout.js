import React, { useEffect } from 'react';
import Cookies from 'js-cookie';
import { useNavigate } from 'react-router-dom';

export default function Logout() {
  const navigate = useNavigate();

  useEffect(() => {
    Cookies.remove('token');
    navigate('/');
  }, [navigate]);

  return (
    <div>
      Sesi anda telah berakhir silahkan lakukan login kembali
      {/* <button onClick={handleClickLogout}>Logout</button> */}
    </div>
  );
}
