import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import api from '../api/api';

export default function ResearchDetail() {
  const params = useParams();
  const [researchDetail, setResearchDetail] = useState([]);

  async function getDetailResearch() {
    try {
      const response = await api.get(`/get-research/${params.id}`);
      const result = response.data.data;
      setResearchDetail(result);
      console.log(result);
    } catch (error) {
      console.error(error);
    }
  }

  useEffect(() => {
    if (params.id) {
      getDetailResearch();
    }
  }, [params]);

  return (
    <div>
      ResearchDetail id : {params.id}
      <div>title : {researchDetail.title_research}</div>
      <div>background : {researchDetail.background_research}</div>
      <div>goals : {researchDetail.goal_research}</div>
      <div>category : {researchDetail.category_research}</div>
      <div>
        researcer :
        <div>
          {researchDetail.researcher?.length === 0 ? (
            <div>Belum Assign Researcher</div>
          ) : (
            <>
              {researchDetail.researcher?.map((researcher, index) => (
                <div key={index}>{researcher?.nama} </div>
              ))}
            </>
          )}
        </div>
      </div>
    </div>
  );
}
