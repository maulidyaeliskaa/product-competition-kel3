export default function Sidebar({ onDashboardClick, onResearchDetailClick, onResearchPhaseClick }) {
  const dashboardClick = () => {
    document.getElementById('dashboardButton').classList.add('sidebarActive');
    document.getElementById('researchPhaseButton').classList.remove('sidebarActive');
    document.getElementById('researchDetailButton').classList.remove('sidebarActive');
  };

  const researchDetailClick = () => {
    document.getElementById('dashboardButton').classList.remove('sidebarActive');
    document.getElementById('researchPhaseButton').classList.remove('sidebarActive');
    document.getElementById('researchDetailButton').classList.add('sidebarActive');
  };

  const researchPhaseClick = () => {
    document.getElementById('researchPhaseButton').classList.add('sidebarActive');
    document.getElementById('dashboardButton').classList.remove('sidebarActive');
    document.getElementById('researchDetailButton').classList.remove('sidebarActive');
  }

  const dashboardCombination = () => {
    dashboardClick();
    onDashboardClick();
  };

  const researchDetailCombination = () => {
    researchDetailClick();
    onResearchDetailClick();
  };

  const researchPhaseCombination = () => {
    researchPhaseClick();
    onResearchPhaseClick();
  }

  return (
    <aside id="IXsW4GPCS0">
      <div id="BKidzY7b8">
        <img src="/assets/images/logo.png"></img>
        <div id="VQVof2cEcc">
          <div className="flex" id="XDEhnZ5hE1">
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path
                d="M9 7H4.6C4.03995 7 3.75992 7 3.54601 7.10899C3.35785 7.20487 3.20487 7.35785 3.10899 7.54601C3 7.75992 3 8.03995 3 8.6V19.4C3 19.9601 3 20.2401 3.10899 20.454C3.20487 20.6422 3.35785 20.7951 3.54601 20.891C3.75992 21 4.03995 21 4.6 21H9M9 21H15M9 21L9 4.6C9 4.03995 9 3.75992 9.10899 3.54601C9.20487 3.35785 9.35785 3.20487 9.54601 3.10899C9.75992 3 10.0399 3 10.6 3L13.4 3C13.9601 3 14.2401 3 14.454 3.10899C14.6422 3.20487 14.7951 3.35785 14.891 3.54601C15 3.75992 15 4.03995 15 4.6V21M15 11H19.4C19.9601 11 20.2401 11 20.454 11.109C20.6422 11.2049 20.7951 11.3578 20.891 11.546C21 11.7599 21 12.0399 21 12.6V19.4C21 19.9601 21 20.2401 20.891 20.454C20.7951 20.6422 20.6422 20.7951 20.454 20.891C20.2401 21 19.9601 21 19.4 21H15"
                stroke="white"
                strokeWidth="2"
                stroke-linecap="round"
                stroke-linejoin="round"
              />
            </svg>
            <p onClick={dashboardCombination} id="dashboardButton" className="sidebarActive">
              {' '}
              Dashboard{' '}
            </p>
          </div>

          <div className="flex" id="XDEhnZ5hE1">
            <svg width="24" height="24" viewBox="0 0 20 22" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path
                d="M18.5 6.27783L9.99997 11.0001M9.99997 11.0001L1.49997 6.27783M9.99997 11.0001L10 20.5001M19 15.0586V6.94153C19 6.59889 19 6.42757 18.9495 6.27477C18.9049 6.13959 18.8318 6.01551 18.7354 5.91082C18.6263 5.79248 18.4766 5.70928 18.177 5.54288L10.777 1.43177C10.4934 1.27421 10.3516 1.19543 10.2015 1.16454C10.0685 1.13721 9.93146 1.13721 9.79855 1.16454C9.64838 1.19543 9.50658 1.27421 9.22297 1.43177L1.82297 5.54288C1.52345 5.70928 1.37369 5.79248 1.26463 5.91082C1.16816 6.01551 1.09515 6.13959 1.05048 6.27477C1 6.42757 1 6.59889 1 6.94153V15.0586C1 15.4013 1 15.5726 1.05048 15.7254C1.09515 15.8606 1.16816 15.9847 1.26463 16.0893C1.37369 16.2077 1.52345 16.2909 1.82297 16.4573L9.22297 20.5684C9.50658 20.726 9.64838 20.8047 9.79855 20.8356C9.93146 20.863 10.0685 20.863 10.2015 20.8356C10.3516 20.8047 10.4934 20.726 10.777 20.5684L18.177 16.4573C18.4766 16.2909 18.6263 16.2077 18.7354 16.0893C18.8318 15.9847 18.9049 15.8606 18.9495 15.7254C19 15.5726 19 15.4013 19 15.0586Z"
                stroke="white"
                strokeWidth="2"
                stroke-linecap="round"
                stroke-linejoin="round"
              />
            </svg>
            <p onClick={researchDetailCombination} id="researchDetailButton" className="researchDetailButton">
              {' '}
              Research Detail{' '}
            </p>
          </div>

          <div className="flex" id="XDEhnZ5hE1">
            <svg width="24" height="24" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path
                fill-rule="evenodd"
                clip-rule="evenodd"
                d="M1.86762 13.4282H4.84462C5.22462 13.4282 5.58062 13.2812 5.84962 13.0122L13.3586 5.50324C13.6666 5.19524 13.8366 4.78524 13.8366 4.34924C13.8366 3.91224 13.6666 3.50124 13.3586 3.19324L12.1416 1.97624C11.5046 1.34124 10.4686 1.34124 9.83062 1.97624L2.35762 9.44924C2.09862 9.70824 1.95162 10.0522 1.94262 10.4172L1.86762 13.4282ZM4.84462 14.9282H1.09862C0.896618 14.9282 0.702618 14.8462 0.561618 14.7012C0.420618 14.5572 0.343618 14.3622 0.348618 14.1592L0.442618 10.3802C0.461618 9.62824 0.764618 8.92124 1.29662 8.38824H1.29762L8.77062 0.915244C9.99262 -0.304756 11.9796 -0.304756 13.2016 0.915244L14.4186 2.13224C15.0116 2.72424 15.3376 3.51124 15.3366 4.34924C15.3366 5.18724 15.0106 5.97324 14.4186 6.56424L6.90962 14.0732C6.35862 14.6242 5.62462 14.9282 4.84462 14.9282Z"
                fill="white"
              />
            </svg>
            <p onClick={researchPhaseCombination} id="researchPhaseButton"> Research Phase </p>
          </div>

          <div className="flex" id="XDEhnZ5hE1">
            <svg width="24" height="24" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path
                d="M7.5 13.6667C7.5 14.9553 8.54467 16 9.83333 16H12C13.3807 16 14.5 14.8807 14.5 13.5C14.5 12.1193 13.3807 11 12 11H10C8.61929 11 7.5 9.88071 7.5 8.5C7.5 7.11929 8.61929 6 10 6H12.1667C13.4553 6 14.5 7.04467 14.5 8.33333M11 4.5V6M11 16V17.5M21 11C21 16.5228 16.5228 21 11 21C5.47715 21 1 16.5228 1 11C1 5.47715 5.47715 1 11 1C16.5228 1 21 5.47715 21 11Z"
                stroke="white"
                strokeWidth="2"
                stroke-linecap="round"
                stroke-linejoin="round"
              />
            </svg>
            <p> Budgeting </p>
          </div>
        </div>
      </div>

      <a href="/" className="flex" id="oq2lJl45ei">
        <img src="/assets/images/Logout.png"></img>
        <p> Sign Out </p>
      </a>
    </aside>
  );
}
