export default function Progress() {
  return (
    <div id="LineDiagram">
      <p>Progress</p>
      <div id="Legend">
        <div id="LegendDetails">
          <svg width="15" height="10" viewBox="0 0 15 10" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path
              fill-rule="evenodd"
              clip-rule="evenodd"
              d="M7.48608 9.05835C11.3521 9.05835 14.4861 7.04363 14.4861 4.55835C14.4861 2.07307 11.3521 0.0583496 7.48608 0.0583496C3.62009 0.0583496 0.486084 2.07307 0.486084 4.55835C0.486084 7.04363 3.62009 9.05835 7.48608 9.05835Z"
              fill="#FFB200"
            />
          </svg>
          <p>Instagram</p>
        </div>
        <div id="LegendDetails">
          <svg width="15" height="10" viewBox="0 0 15 10" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path
              fill-rule="evenodd"
              clip-rule="evenodd"
              d="M7.48608 9.05835C11.3521 9.05835 14.4861 7.04363 14.4861 4.55835C14.4861 2.07307 11.3521 0.0583496 7.48608 0.0583496C3.62009 0.0583496 0.486084 2.07307 0.486084 4.55835C0.486084 7.04363 3.62009 9.05835 7.48608 9.05835Z"
              fill="#4339F2"
            />
          </svg>
          <p>Facebook</p>
        </div>
        <div id="LegendDetails">
          <svg width="15" height="10" viewBox="0 0 15 10" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path
              fill-rule="evenodd"
              clip-rule="evenodd"
              d="M7.48608 9.05835C11.3521 9.05835 14.4861 7.04363 14.4861 4.55835C14.4861 2.07307 11.3521 0.0583496 7.48608 0.0583496C3.62009 0.0583496 0.486084 2.07307 0.486084 4.55835C0.486084 7.04363 3.62009 9.05835 7.48608 9.05835Z"
              fill="#02A0FC"
            />
          </svg>
          <p>Twitter</p>
        </div>
        <div id="LegendDetails">
          <svg width="15" height="10" viewBox="0 0 15 10" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path
              fill-rule="evenodd"
              clip-rule="evenodd"
              d="M7.48608 9.05835C11.3521 9.05835 14.4861 7.04363 14.4861 4.55835C14.4861 2.07307 11.3521 0.0583496 7.48608 0.0583496C3.62009 0.0583496 0.486084 2.07307 0.486084 4.55835C0.486084 7.04363 3.62009 9.05835 7.48608 9.05835Z"
              fill="#FF3A29"
            />
          </svg>
          <p>Google</p>
        </div>
      </div>
      <div className="all">
        <div className="circle-wrap">
          <div className="circle">
            <div className="mask full-1">
              <div className="fill-1"></div>
            </div>
            <div className="mask half">
              <div className="fill-1"></div>
            </div>
            <div className="inside-circle"> 67% </div>
          </div>
        </div>
        <div className="circle-wrap">
          <div className="circle">
            <div className="mask full-2">
              <div className="fill-2"></div>
            </div>
            <div className="mask half">
              <div className="fill-2"></div>
            </div>
            <div className="inside-circle"> 46% </div>
          </div>
        </div>
        <div className="circle-wrap">
          <div className="circle">
            <div className="mask full-3">
              <div className="fill-3"></div>
            </div>
            <div className="mask half">
              <div className="fill-3"></div>
            </div>
            <div className="inside-circle"> 15% </div>
          </div>
        </div>
        <div className="circle-wrap">
          <div className="circle">
            <div className="mask full-4">
              <div className="fill-4"></div>
            </div>
            <div className="mask half">
              <div className="fill-4"></div>
            </div>
            <div className="inside-circle"> 67% </div>
          </div>
        </div>
      </div>
      <div className="caption">
        <p>
          Every large design company whether it’s a multi-national branding corporation or a regular down at heel tatty magazine publisher needs to
          fill holes in the workforce.
        </p>
      </div>
    </div>
  );
}
