import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import api from "../api/api";

export default function Slider() {
  const [listResearch, setListResearch] = useState(null);
  const [isLoading, setIsLoading] = useState(false);

  async function getListResearch() {
    setIsLoading(true);
    try {
      const response = await api.get("/get-research");
      const result = response.data.data;
      setListResearch(result);
    } catch (error) {
      console.error(error);
    } finally {
      setIsLoading(false);
    }
  }

  useEffect(() => {
    getListResearch();
  }, []);

  if (isLoading) return <p>Loading ....</p>;

  return (
    <div className="wrapper">
      <div className="button-wrapper">
        <button className="button-more">
          More <img src="/assets/images/right-arrow.png"></img>
        </button>
      </div>
      <h2>On Going Research</h2>

      <ul className="carousel">
        {listResearch?.map((item) => (
          <Link to={`/research-detail/${item.id_research}`}>
            <li key={item.id_research} className="card">
              <div>
                <img src="/assets/images/Image.png" alt={item.phase} />
                <h3>{item.title_research}</h3>
                <p>{item.background_research}</p>
                {item.researcher.map((researcher, index) => (
                  <img
                    key={index}
                    className="photo"
                    src="/assets/images/Photo3.png"
                    alt={researcher.nama}
                  />
                ))}
              </div>
            </li>
          </Link>
        ))}
      </ul>
    </div>
  );
}
