import ResearchPhaseCard from "./ResearchPhaseCard";

export default function ResearchPhaseMain({ onResearchPhaseDetailClick }){
    return(
        <>
            <div id="ResearchPhaseMain">
                <ResearchPhaseCard onResearchPhaseDetailClick={onResearchPhaseDetailClick}/>
                <ResearchPhaseCard onResearchPhaseDetailClick={onResearchPhaseDetailClick}/>
                <ResearchPhaseCard onResearchPhaseDetailClick={onResearchPhaseDetailClick}/>
                <ResearchPhaseCard onResearchPhaseDetailClick={onResearchPhaseDetailClick}/>
                <ResearchPhaseCard onResearchPhaseDetailClick={onResearchPhaseDetailClick}/>
                <ResearchPhaseCard onResearchPhaseDetailClick={onResearchPhaseDetailClick}/>
            </div>
        </>
    )
}